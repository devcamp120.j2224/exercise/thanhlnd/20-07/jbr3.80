package com.devcamp.jbr380.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OddEvenNumberAPI {
    @CrossOrigin
    @GetMapping(value = "/checknumber")
    public String checknumber(@RequestParam(value = "number",defaultValue = "1")int number) {
        if ( number % 2 == 0 )
        return ("Số nhập vào là số chẵn");
        else
        return ("Số nhập vào là số lẻ");
    };

}
